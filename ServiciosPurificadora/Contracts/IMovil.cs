﻿using ServiciosPurificadora.Entity;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ServiciosPurificadora.Contracts
{
    [ServiceContract]
    public interface IMovil
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientes", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<ClienteEntity> GetClientes();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "NewCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int NewCliente(string nombre, long telefono, string colonia, string calle_numero, string referencias);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClientesNombre", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        long GetClientesNombre(string nombre);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "NewUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int NewUsuario(long id_Cliente, string nombre_Usuario, string password, bool activo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetLogin", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetLogin(string user, string pass);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "GetClienteDatos", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        ClienteEntity GetClienteDatos(string nombre_usuario);


        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdateCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int UpdateCliente(string nombre_usuario, string nombre, long telefono, string colonia, string calle_numero, string referencias);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "UpdatePassword", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int UpdatePassword(string nombre_usuario, string Password);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "sendPedido ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int sendPedido(long id_Cliente, int cantidad, string comentarios, string fecha);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ValidaPedido ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool ValidaPedido(long id_Cliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "sendUsuarioToken ", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        int sendUsuarioToken(string Usuario, string token);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "getPedidodsCliente", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        List<PedidosEntity> getPedidodsCliente(long id_Cliente);





    }


}