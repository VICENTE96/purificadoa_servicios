﻿using ServiciosPurificadora.Contracts;
using ServiciosPurificadora.Controllers;
using ServiciosPurificadora.Entity;
using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using System.Web.Services;


namespace ServiciosPurificadora
{
    [ScriptService]
    public partial class WebService1 : IMovil
    {

        ServiciosMovilController serviciosMovilController = new ServiciosMovilController();

        [WebMethod]
        public List<ClienteEntity> GetClientes()
        {
            //if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return serviciosMovilController.GetClientes();
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public int NewCliente(string nombre, long telefono, string colonia, string calle_numero, string referencias)
        {
            //if (WebOperationContext.Current.IncomingRequest.Method == "OPTIONS") return null;
            try
            {

                return serviciosMovilController.NewCliente(nombre,telefono,colonia,calle_numero,referencias);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public long GetClientesNombre(string nombre)
        {
           
            try
            {

                return serviciosMovilController.GetClientesNombre(nombre);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public int NewUsuario(long id_Cliente, string nombre_Usuario, string password, bool activo)
        {
            
            try
            {

                return serviciosMovilController.NewUsuario( id_Cliente,nombre_Usuario,password,activo);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public string GetLogin(string user, string pass)
        {

            try
            {

                return serviciosMovilController.GetLogin(user, pass);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public ClienteEntity GetClienteDatos(string nombre_usuario)
        {

            try
            {

                return serviciosMovilController.GetClienteDatos( nombre_usuario);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public int UpdateCliente(string nombre_usuario, string nombre, long telefono, string colonia, string calle_numero, string referencias)
        {

            try
            {

                return serviciosMovilController.UpdateCliente(nombre_usuario, nombre, telefono, colonia, calle_numero, referencias);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public int UpdatePassword(string nombre_usuario, string Password)
        {
            try
            {

                return serviciosMovilController.UpdatePassword(nombre_usuario, Password);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public int sendPedido(long id_Cliente, int cantidad, string comentarios, string fecha)
        {
            try
            {

                return serviciosMovilController.sendPedido(id_Cliente,cantidad,comentarios,fecha);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public bool ValidaPedido(long id_Cliente)
        {
            try
            {

                return serviciosMovilController.ValidaPedido( id_Cliente);
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public int sendUsuarioToken(string Usuario, string token)

        {
            try
            {

                return serviciosMovilController.sendUsuarioToken( Usuario,token)
;
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        [WebMethod]
        public List<PedidosEntity> getPedidodsCliente(long id_Cliente)

        {
            try
            {

                return serviciosMovilController.getPedidodsCliente(id_Cliente)
;
            }
            catch (Exception ex)
            {
                throw new WebFaultException<string>(ex.Message, HttpStatusCode.InternalServerError);
            }
        }


    }
}
