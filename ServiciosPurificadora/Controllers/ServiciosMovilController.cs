﻿using ServiciosPurificadora.Entity;
using ServiciosPurificadora.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServiciosPurificadora.Controllers
{
    public class ServiciosMovilController
    {
        public List<ClienteEntity> GetClientes()
        {
            List<ClienteEntity> lista = new List<ClienteEntity>();
            try
            {
                dbHelper db = new dbHelper();
                //db.agregarParametro("@CONTRATO", SqlDbType.VarChar, Contrato);
                SqlDataReader rd = db.consultaReader("getClientes");
                while (rd.Read())
                {
                    ClienteEntity lista1 = new ClienteEntity();
                    lista1.id_Cliente = long.Parse(rd[0].ToString());
                    lista1.nombre = rd[1].ToString();
                    lista1.telefono = long.Parse(rd[2].ToString());
                    lista1.colonia = rd[3].ToString();
                    lista1.calle_numero = rd[4].ToString();
                    lista1.referencias = rd[5].ToString();
                    lista.Add(lista1);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) { }

            return lista;
        }



        public int NewCliente(string nombre,long telefono, string colonia, string calle_numero, string referencias)
        {
           // List<ClienteEntity> lista = new List<ClienteEntity>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@telefono", SqlDbType.BigInt, telefono);
                db.agregarParametro("@colonia", SqlDbType.VarChar, colonia);
                db.agregarParametro("@calle_numero", SqlDbType.VarChar, calle_numero);
                db.agregarParametro("@referencias", SqlDbType.VarChar, referencias);
                SqlDataReader rd = db.consultaReader("insertar_Cliente");
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) {
                throw new Exception("Error al agregar nuevo Cliente " + ex.Message, ex);
            }

            return 1;
        }

        public long  GetClientesNombre(string nombre)
        {
            long result = 0;
            try
            {   
                dbHelper db = new dbHelper();
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                SqlDataReader rd = db.consultaReader("getClientesId");
                while (rd.Read())
                {
                    result = long.Parse(rd[0].ToString());
                   
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
                

            }
            catch (Exception ex) {
                throw new Exception("Error al consgir id Cliente " + ex.Message, ex);
            }

            return result;
        }

        public int NewUsuario(long id_Cliente, string nombre_Usuario, string password, bool activo)
        {
            //List<ClienteEntity> lista = new List<ClienteEntity>();
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id_Cliente", SqlDbType.Int, id_Cliente);
                db.agregarParametro("@nombre_usuario", SqlDbType.VarChar, nombre_Usuario);
                db.agregarParametro("@password", SqlDbType.VarChar, password);
                db.agregarParametro("@activo", SqlDbType.Bit, activo);
                SqlDataReader rd = db.consultaReader("insertar_Usuario");
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al agregar nuevo usuario" + ex.Message, ex);
            }

            return 1;
        }

        public string GetLogin(string user, string pass)
        {
            string result = " ";
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@user", SqlDbType.VarChar, user);
                db.agregarParametro("@pass", SqlDbType.VarChar, pass);
                SqlDataReader rd = db.consultaReader("login");
                while (rd.Read())
                {
                    result = rd[0].ToString();

                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error de autenticación" + ex.Message, ex);
            }

            return result;
        }

        public ClienteEntity GetClienteDatos(string nombre_usuario)
        {
            ClienteEntity lista = new ClienteEntity();

            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@nombre_usuario", SqlDbType.VarChar, nombre_usuario);
                SqlDataReader rd = db.consultaReader("consulta_domicilio");
                while (rd.Read())
                {
                    lista.id_Cliente = long.Parse(rd[0].ToString());
                    lista.nombre = (rd[1].ToString());
                    lista.telefono = long.Parse(rd[2].ToString());
                    lista.colonia = (rd[3].ToString());
                    lista.calle_numero = (rd[4].ToString());
                    lista.referencias = (rd[5].ToString());


                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();


            }
            catch (Exception ex)
            {
                throw new Exception("Error al consgir datos del cliente " + ex.Message, ex);
            }

            return lista;
        }

        public int UpdateCliente(string nombre_usuario, string nombre, long telefono, string colonia, string calle_numero, string referencias)
        {
            // List<ClienteEntity> lista = new List<ClienteEntity>();
            try
            {
                dbHelper db = new dbHelper();
             
                db.agregarParametro("@nombre_usuario", SqlDbType.VarChar, nombre_usuario);
                db.agregarParametro("@nombre", SqlDbType.VarChar, nombre);
                db.agregarParametro("@telefono", SqlDbType.BigInt, telefono);
                db.agregarParametro("@colonia", SqlDbType.VarChar, colonia);
                db.agregarParametro("@calle_numero", SqlDbType.VarChar, calle_numero);
                db.agregarParametro("@referencias", SqlDbType.VarChar, referencias);

                SqlDataReader rd = db.consultaReader("Actualizar_Cliente");
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al agregar nuevo Cliente " + ex.Message, ex);
            }

            return 1;
        }


        public int UpdatePassword(string nombre_usuario, string Password)
        {
           
            try
            {
                dbHelper db = new dbHelper();

                db.agregarParametro("@nombre_usuario", SqlDbType.VarChar, nombre_usuario);
                db.agregarParametro("@Password", SqlDbType.VarChar, Password);
               
                SqlDataReader rd = db.consultaReader("Cambio_Password");
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al cambiar contraseña " + ex.Message, ex);
            }

            return 1;
        }

        public int sendPedido (long id_Cliente, int cantidad, string comentarios, string fecha)
        {
      
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id_Cliente", SqlDbType.Int, id_Cliente);
                db.agregarParametro("@cantidad_Pedido", SqlDbType.Int, cantidad);
                db.agregarParametro("@comentarios", SqlDbType.VarChar, comentarios);
                db.agregarParametro("@fechaPedido", SqlDbType.DateTime,DateTime.Parse( fecha));
                SqlDataReader rd = db.consultaReader("insertaPedido");
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al enviar pedido" + ex.Message, ex);
            }

            return 1;
        }

        public bool ValidaPedido(long id_Cliente)
        {
            bool result = false;
            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id_Cliente", SqlDbType.BigInt, id_Cliente);
         
                SqlDataReader rd = db.consultaReader("valida_Pedido");
                while (rd.Read())
                {
                    result = bool.Parse(rd[0].ToString());

                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error de validación" + ex.Message, ex);
            }

            return result;
        }

        public int sendUsuarioToken(string Usuario, string token)
        {

            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@Usuario", SqlDbType.VarChar, Usuario);
                db.agregarParametro("@token", SqlDbType.VarChar, token);
             
                SqlDataReader rd = db.consultaReader("inserta_UsuarioToken");
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al enviar token" + ex.Message, ex);
            }

            return 1;
        }

        public List<PedidosEntity> getPedidodsCliente( long id_Cliente)
        {
            List<PedidosEntity> lista = new List<PedidosEntity>();

            try
            {
                dbHelper db = new dbHelper();
                db.agregarParametro("@id_Cliente", SqlDbType.VarChar, id_Cliente);
                SqlDataReader rd = db.consultaReader("consultaPedidosCliente");
                while (rd.Read())
                {
                    PedidosEntity lista1 = new PedidosEntity();
                    lista1.id_Pedido = long.Parse(rd[0].ToString());
                    lista1.id_Cliente =long.Parse(rd[1].ToString());
                    lista1.cantidad_Pedido = long.Parse(rd[2].ToString());
                    lista1.costo = decimal.Parse(rd[3].ToString());
                    lista1.comentarios = rd[4].ToString();
                    lista1.fecha_Pedido = DateTime.Parse(rd[5].ToString()).ToString("dddd, dd MMMM yyyy HH:mm");
                    lista.Add(lista1);
                }
                rd.Close();
                db.conexion.Close();
                db.conexion.Dispose();
            }
            catch (Exception ex) {
                throw new Exception("Error: " + ex.Message, ex);
            }

            return lista;
        }




    }
}