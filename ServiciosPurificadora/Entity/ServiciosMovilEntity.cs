﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosPurificadora.Entity
{
 
        public class ClienteEntity
        {
            public long id_Cliente { get; set; }
            public string nombre { get; set; }
            public long telefono { get; set; }
            public string colonia { get; set; }
            public string calle_numero { get; set; }
            public string referencias { get; set; }


        }

    public class PedidosEntity
    {
        public long id_Pedido { get; set; }
        public long id_Cliente { get; set; }
        public long cantidad_Pedido { get; set; }
        public decimal costo { get; set; }
        public string comentarios { get; set; }
        public string fecha_Pedido { get; set; }


    }


}